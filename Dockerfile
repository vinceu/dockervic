FROM centos:6

RUN yum -y update 
RUN rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
RUN yum -y install ansible
ADD test.conf /etc/httpd/conf.d/test.conf
ADD index.php /mnt/index.php
ADD playbook.yml /tmp/playbook.yml
RUN ansible-playbook /tmp/playbook.yml -c local

EXPOSE 80
ENTRYPOINT ["/usr/sbin/httpd"]
CMD ["-D", "FOREGROUND"]
